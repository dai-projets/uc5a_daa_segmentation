#
# Copyright (c) Søren Rasmussen 2020 -- 2021.
# Copyright (c) The Danish Agricultural Agency 2020 -- 2021.
# This file belongs to subproject uc5a_daa_segmentation of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#

import numpy as np
import torch as th

def ifnone(x,default):
    return default if x is None else x

def rint(x):
    #Rounding integer conversion
    if isinstance(x, np.ndarray):
        return np.round(x).astype(np.int)
    else:
        return int(round(x))

def chans_first(x, batch=False):
    '''
      Permute `x` to channels-first (assuming it's channels-last)
      
      Parameters
      ----------
        x : np.ndarray or th.Tensor
          Data
        batch : Bool
          `True` if the first dimension of `x` should be treated as
          a batch dimension and left alone. Default. `False`
    '''
    order = (x.ndim-1,*np.arange(batch,x.ndim-1))
    if batch: order = (0,*order)
    if isinstance(x, np.ndarray):
        return np.transpose(x, order)
    elif isinstance(x, th.Tensor):
        return x.permute(*order)
    else:
        raise ValueError("Valid types: np.ndarray and torch.Tensor")

def chans_last(x, batch=False):
    '''
      Permute `x` to channels-last (assuming it's channels-first)
      
      Parameters
      ----------
        x : np.ndarray or th.Tensor
          Data
        batch : Bool
          `True` if the first dimension of `x` should be treated as
          a batch dimension and left alone. Default. `False`
    '''
    order = (*np.arange(batch+1,x.ndim), int(batch))
    if batch: order = (0,*order)
    if isinstance(x, np.ndarray):
        return np.transpose(x, order)
    elif isinstance(x, th.Tensor):
        return x.permute(*order)
    else:
        raise ValueError("Valid types: np.ndarray and torch.Tensor")


def affine2shapely(T):
    #Affine transform to shapely format
    return (T.a, T.b, T.d, T.e, T.xoff, T.yoff)
def affine2mtx(T):
    #Affine transform to Numpy array
    return np.array(T).reshape(3,3)
