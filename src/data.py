#
# Copyright (c) Søren Rasmussen 2020 -- 2021.
# Copyright (c) The Danish Agricultural Agency 2020 -- 2021.
# This file belongs to subproject uc5a_daa_segmentation of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#

import shapely.geometry as sg
import shapely.affinity as saff
import numpy as np
from numbers import Number
from affine import Affine
import copy
import torch as th
import rasterio as rio
from .utils import ifnone, chans_first, affine2shapely, affine2mtx
from .vector import *

RESAMPLING = rio.enums.Resampling

__all__ = ['Patch', 'PatchSampler', 'PatchPair', 'PatchPairSampler']

def _pointcoords(p):
    return (p.x, p.y) if isinstance(p, sg.Point) else p

def to_tuple(x,twosided=False):
    #Transform input to a range tuple
    if x is None: return None
    elif isinstance(x,(list,tuple,np.ndarray)) and len(x) == 2:
        return tuple(x)
    elif isinstance(x, (Number, np.ndarray)):
        return ((-1)**twosided*x,x)
    else:
        raise ValueError("`x` must be a list/tuple/ndarray of length 2 or a single number.")

def log_uniform(low=0.0, high=1.0, size=None):
    #Log-uniform random
    return np.exp(np.random.uniform(np.log(low),np.log(high),size))
        
def sample_range(x, size=None, randfun=None):
    '''
      Sample in the range [x[0],x[1]]
      If x is a number, behave like x = [x,x]
      If randfun is set, sample using this function.
      Else sample using np.random.uniform.
    '''
    if isinstance(x, Number): return np.ones(size)*x
    if not isinstance(x, tuple): x = to_tuple(x, True)
    if randfun: return randfun(*x, size)
    else:       return np.random.uniform(*x, size)

class Patch():
    '''
      Raster patch class. Used to define and read patches in rasters.
      
      Parameters
      ----------
      x,y : floats. patch center geo-coordinates
      w,h : floats. patch geo-width, geo-height
      r : float. patch rotation (counter-clockwise, degrees)
      sx,sy : floats. patch shearing
      fx,fy : bools. Horizontal and vertical flip
      out_hw : int or tuple(int,int).
          Default output raster hwight and width.
          Default value: `None` (meaning `out_hw` is specified on when calling `Patch.read(...)`.
      bounds : (x_low,y_low,x_high,y_high)
        Can be used to define an axis-aligned patch instead of using `x,y,w,h,r,sx,sy,fx,fy`.  
      src, size : See `Patch.read()`
      eager : Bool (Default: False)
        Read patch data on initialization and store in cache?
        Requires `src` and `out_hw` at init
    
      Attributes
      ----------
      poly : Returns a shapely polygon for the patch
      G : Patch geotransform
      geotfm(out_hw) : Patch geotransform for non-default output shape.
      area : Patch area
      bounds : Axis-aligned bounds
      read(src=None, out_hw=None,**kwargs) : 
        Read raster from `src` with output shape `out_hw`.
        Will use the values given at initialization, if not set.
      crs : Source crs

    '''
    def __init__(self,x=0,y=0,w=1,h=1,r=0,sx=0,sy=0,fx=False,fy=False,out_hw=None,bounds=None,src=None,res=None,eager=False):
        if bounds:
            assert (x,y,w,h,r,sx,sy)==(0,0,1,1,0,0,0), "`bounds` should not be combined with other parameters."
            x, y = (bounds[2]+bounds[0])/2, (bounds[3]+bounds[1])/2
            w, h = bounds[2]-bounds[0], bounds[3]-bounds[1]
        self.xy,self.wh,self.r,self.sx,self.sy,self.fx,self.fy = (x,y),(w,h),r,sx,sy,fx,fy
        self.src = src
        self.out_hw = to_tuple(out_hw)
        self.eager = eager
        self._data = None
        if self.eager: self.read()

    def __repr__(self):
        return f"Patch (x,y)={self.xy}, (w,h)={self.wh}, r={self.r}, s={(self.sx,self.sy)}, f={(int(self.fx),int(self.fy))}"
    
    @property
    def poly(self):
        return saff.affine_transform(sg.box(0,0,self.out_w, self.out_h), affine2shapely(self.G))
    
    @property
    def G(self):
        return self.geotfm()

    def geotfm(self, out_hw=None):
        oh,ow = to_tuple(out_hw) if out_hw is not None else self.out_hw
        """ Get patch geotransform """
        T1 = Affine.translation(-ow/2,-oh/2)
        F = Affine.scale(1-2*self.fx, 1-2*self.fy)
        H = Affine.shear(self.sx,self.sy)
        R = Affine.rotation(self.r)
        S = Affine.scale(self.w/ow, -self.h/oh)
        T2 = Affine.translation(self.x,self.y)
        G = T2*S*R*H*F*T1
        return G

    @property
    def area(self): return self.poly.area

    @property
    def bounds(self): return self.poly.bounds
              
    def read(self, src=None, out_hw=None, **kwargs):
        if self.eager and self._data is not None and (src,out_hw, kwargs)==(None,None, {}):
            return self._data
        src = src or self.src
        out_hw = out_hw or self.out_hw
        out = src.read(self.geotfm(out_hw), out_hw, **kwargs)
        if self.eager:
            self._data = out
        return out
    
    @property
    def crs(self): return self.src.crs
    @property
    def params(self): return (*self.xy, *self.wh, self.r, self.sx, self.sy, self.fx, self.fy)
    def __iter__(self): return iter(self.params) #Allows unpacking using the form (x,y,w,h,r,sx,sy,fx,fy) = patch
    @property
    def xy(self): return (self.x, self.y)
    @xy.setter
    def xy(self,xy): self.x, self.y = xy
    @property
    def yx(self): return (self.y, self.x)
    @yx.setter
    def yx(self,yx): self.y, self.x = yx
    @property
    def hw(self): return (self.h, self.w)
    @hw.setter
    def hw(self,hw): self.h, self.w = hw
    @property
    def wh(self): return (self.w, self.h)
    @wh.setter
    def wh(self,wh): self.w, self.h = wh

    @property
    def out_w(self): return self.out_hw[1]
    @out_w.setter
    def out_w(self,w): self.out_hw = (self.out_hw[0], w)
    @property
    def out_h(self): return self.out_hw[0]
    @out_h.setter
    def out_h(self,h): self.out_hw = (h, self.out_hw[1])

class PatchSampler(th.utils.data.Dataset):
    '''
      Random sampling of patches.

      Parameters
      ----------
        src, out_hw : See `Patch.read()`
        pointsampler_spec : See `get_pointsampler`
        extent : val or tuple(min_val, max_val)
          Patch width and height will be sampled in the range [min_val;max_val].
        rotation : val or tuple(min_val, max_val)
          Rotation will be sampled in the range [min_val;max_val].
          The unit is degrees.
        translation : val or tuple(min_val, max_val):
          offset from the sampeled point will be sampeled in the range
          [min_val, max_val]
        shear : val or tuple(min_val, max_val)
          Rotation will be sampled in the range [min_val;max_val].
          The unit is degrees.
        flip : probability or tuple(prob_horz, prob_vert)
          Probability of flipping the image. Range: [0,1]
        eager : Bool
          See Patch.eager.
        transform : Callable
          Function to transform the patch

      Attributes
      ----------
      __call__(idx=None, c=None, apply_transform=True) :
        Sample patch centered at `c`.
        If `c` is `None`, `c` is selected using the point sampler`.
        Further, if `idx` is specified, then the point sampler will sample
          from its `idx`'th element.
        Whether to apply `self.transform` to the output is determined by `apply_transform`.
    '''
    def __init__(self,
                 src=None, out_hw=None,          #Raster params
                 pointsampler_spec={}, #See get_pointsampler()
                 extent=None, rotation=None, translation=None, shear=None, #Spatial augmentation
                 flip=None, relative_rotation=None, #Spatial augmentation
                 eager=False,
                 transform=None,
                 len=None):
        self.src, self.out_hw = src, to_tuple(out_hw)
        self.pointsampler = get_pointsampler(**pointsampler_spec)
        self.extent, self.rotation, self.translation, self.shear = extent, rotation, translation, shear
        self.flip = flip
        self.relative_rotation = relative_rotation
        self.eager = eager
        self.transform = transform
        self.len = len
        self.worker_initialized = False

    @staticmethod
    def worker_init_fn(wid): #Used to init pytorch DataLoader workers
        winfo = th.utils.data.get_worker_info()
        if winfo is not None:
            np.random.seed(winfo.seed % (2**32-1)) #Numpy does not support Long seeds
    
    #Can be passed as default arguments when initializing a pytorch DataLoader
    dl_kwargs = dict(worker_init_fn=worker_init_fn.__func__, pin_memory=True, shuffle=False, drop_last=True)

    def initialize_worker(self,wid=None):
        if th.utils.data.get_worker_info() is not None:
            self.worker_init_fn(wid)
            self.worker_initialized = True
    
    def __len__(self):
        return self.len or len(self.pointsampler)

    def _apply_transform(self, p):
        return self.transform(p) if self.transform is not None else p

    def __call__(self, idx=None, c=None, apply_transform=True):
        if not self.worker_initialized: self.initialize_worker()
        if self.len: idx = None
        cx, cy = _pointcoords(self.pointsampler(idx) if c is None else c)
        w, h   = 100*np.ones(2)
        r      = np.zeros(1)
        hx, hy = np.zeros(2)
        fh, fv = np.zeros(2, dtype=np.bool)
        if self.extent is not None: #Note the log_uniform sampling
            w, h = sample_range(self.extent, 2, randfun=log_uniform)
        if self.rotation is not None:
            r = sample_range(self.rotation, 1)
        if self.translation is not None:
            tx, ty = sample_range(self.translation, 2)
            cx, cy = cx+tx,cy+ty
        if self.shear is not None:
            hx, hy = sample_range(self.shear, 2)
        if self.flip is not None:
            fh, fv = sample_range((0,1),2) < self.flip
        
        p = Patch(cx, cy, w, h, r, hx, hy, fh, fv, src=self.src,out_hw=self.out_hw,eager=self.eager)
        return self._apply_transform(p) if apply_transform else p
    
    def __getitem__(self, idx):
        return self(idx)

class PatchPair():
    '''
      Pair of `Patch`.
      Used to hold a pair of (overlapping) patches and
      transform the contents of one to the image space of the other
      
      Parameters
      ----------
      a,b : `Patch`es
      src, out_hw : See `PatchPair.read()`

      Attributes
      ----------
      poly : Returns a shapely MultiPolygon for the patchs
      union : Union of patches
      intersection : Intersection of patches
      overlap :
        Return the ratio of the intersection area to `self.a.area`.
      swap(inplace=False):
        Swap `self.a` and `self.b` and returns the resulting PatchPair.
        If inplace==False, a new PatchPair is created.
      read(src=None, out_hw=None,**kwargs) : 
        See `Patch.read()`

        Returns
        -------
        img_a : Raster data for `a`
        img_b : Raster data for `b`
        tfm_fun(size_hw, pytorch=False) : A function which returns
          the img_b-to-img_a transform matrix for data of size `size_hw`
          or normalized if `pytorch==True`, as well as a binary mask of
          matching shape for the transformed image.
    '''
    def __init__(self,a,b, src=None, out_hw=None):
        self.a, self.b = a,b
        self.src, self.out_hw = src, out_hw

    @property
    def poly(self): return sg.MultiPolygon([self.a.poly, self.b.poly])
    @property
    def union(self): return self.a.poly.union(self.b.poly)
    @property
    def intersection(self): return self.a.poly.intersection(self.b.poly)
    @property
    def overlap(self):
        return self.intersection.area/self.a.area

    def swap(self, inplace=True):
        if inplace:
            self.a, self.b = (self.b, self.a)
            return self
        else:
            out = copy.deepcopy(self)
            out.a, out.b = (out.b, out.a)
            if out._data is not None:
                out._data = (out._data[1], out._data[0], *out._data[2:])

    def read(self, src=None, out_hw=None, **kwargs):
        src = src or self.src
        out_hw = out_hw or self.out_hw
        img_a = self.a.read(src=src, out_hw=out_hw, **kwargs)
        img_b = self.b.read(src=src, out_hw=out_hw, **kwargs)


        def tfm_fun(hw=None, pytorch=False):
            if hw and not pytorch:
                M = ~self.b.geotfm(out_hw=hw)*self.a.geotfm(out_hw=hw)
            else:
                M = ~self.b.G*self.a.G
                if pytorch:
                    T = Affine.translation(-1,-1)
                    S = Affine.scale(2/(out_hw[1]-1), 2/(out_hw[0]-1))
                    P = T*S
                    M = np.array(P*M*~P)[:6].reshape(2,3)          

            mask = np.ones(hw or out_hw, dtype=np.uint8)
            rio.warp.reproject(
                source        = mask,
                src_transform = self.b.geotfm(out_hw=hw),
                src_crs       = self.b.crs,
                destination   = mask,
                dst_transform = self.a.geotfm(out_hw=hw),
                dst_crs       = self.b.crs,
                resampling    = RESAMPLING.nearest,
            )
            return M, mask
        return (img_a, img_b, tfm_fun)
        
class PatchPairSampler(th.utils.data.Dataset):
    '''
      Random sampling of overlapping patch pairs.

      Parameters
      ----------
        src, out_hw : See `PatchPair.read()`
        pointsampler_spec : See `get_pointsampler`
        extent : val or tuple(min_val, max_val)
          Patch width and height will be sampled in the range [min_val;max_val].
        rotation : val or tuple(min_val, max_val)
          Rotation will be sampled in the range [min_val;max_val].
          The unit is degrees.
        translation : val or tuple(min_val, max_val)
          Translation will be sampled in the range [min_val;max_val].
          The unit is given by the CRS of `src`.
        shear : val or tuple(min_val, max_val)
          Rotation will be sampled in the range [min_val;max_val].
          The unit is degrees.
        flip : probability or tuple(prob_horz, prob_vert)
          Probability of flipping the image. Range: [0,1]
        relative_rotation : None or list[int]
          Relative rotations to select from.
          If `None` then rot_b is independent from rot_a.
          Else, `rot_b = rot_a + random_choice(relative_rotation)`
        eager : Bool
          See Patch.eager.
        transform : Callable
          Transform function, applied to each patch in the pair.

      Attributes
      ----------
      __call__(idx=None, c=None, apply_transform=True) :
        Sample patch pair centered at `c`.
        If `c` is `None`, `c` is selected using the point sampler`.
        Further, if `idx` is specified, then the point sampler will sample
          from its `idx`'th element.
        Whether to apply `self.transform` to the output is determined by `apply_transform`.
    '''

    
    def __init__(self,
                 src=None, out_hw=None,          #Raster params
                 pointsampler_spec={}, #See get_pointsampler()
                 extent=None, rotation=None, translation=None, shear=None, #Spatial augmentation
                 flip=None, relative_rotation=None, #Spatial augmentation
                 eager=False,
                 transform=None,
                 len=None):
        self.src, self.out_hw = src, to_tuple(out_hw)
        self.pointsampler = get_pointsampler(**pointsampler_spec)
        self.extent, self.rotation, self.translation, self.shear = extent, rotation, translation, shear
        self.flip = flip
        self.relative_rotation = relative_rotation
        self.eager = eager
        self.transform = transform
        self.len = len
        self.worker_initialized = False

    @staticmethod
    def worker_init_fn(wid): #Used to init pytorch DataLoader workers
        winfo = th.utils.data.get_worker_info()
        if winfo is not None:
            np.random.seed(winfo.seed % (2**32-1)) #Numpy does not support Long seeds
    
    #Can be passed as default arguments when initializing a pytorch DataLoader
    dl_kwargs = dict(worker_init_fn=worker_init_fn.__func__, pin_memory=True, shuffle=False, drop_last=True)

    def initialize_worker(self,wid=None):
        if th.utils.data.get_worker_info() is not None:
            self.worker_init_fn(wid)
            self.worker_initialized = True
    
    def __len__(self):
        return self.len or len(self.pointsampler)

    def _apply_transform(self, pp):
        return self.transform(pp) if self.transform is not None else pp

    def __call__(self, idx=None, c=None, apply_transform=True):
        if not self.worker_initialized: self.initialize_worker()
        if self.len: idx = None
        cx, cy = _pointcoords(self.pointsampler(idx) if c is None else c)
        w, h   = 100*np.ones((2,2))
        r      = np.zeros(2)
        tx, ty = np.zeros((2,2))
        sx, sy = np.zeros((2,2))
        af, bf = np.zeros((2,2), dtype=np.bool)
        if self.extent is not None: #Note the log_uniform sampling
            w, h = sample_range(self.extent, (2,2), randfun=log_uniform)
        if self.rotation is not None:
            r = sample_range(self.rotation, 2)
        if self.relative_rotation is not None:
            r[1] = r[0] + np.random.choice(self.relative_rotation)
        if self.translation is not None:
            tx, ty = sample_range(self.translation, (2,2))
        if self.shear is not None:
            sx, sy = sample_range(self.shear, (2,2))
        if self.flip is not None:
            af, bf = sample_range((0,1),(2,2)) < self.flip
        

        x,y = cx+tx, cy+ty
        pa = Patch(x=x[0],y=y[0],w=w[0],h=h[0],r=r[0],sx=sx[0],sy=sy[0],fx=af[0],fy=af[1],
                   src=self.src, out_hw=self.out_hw, eager=self.eager)
        pb = Patch(x=x[1],y=y[1],w=w[1],h=h[1],r=r[1],sx=sx[1],sy=sy[1],fx=bf[0],fy=bf[1],
                   src=self.src, out_hw=self.out_hw, eager=self.eager)
        
        res = PatchPair(pa,pb,src=self.src,out_hw=self.out_hw)
        return self._apply_transform(res) if apply_transform else res
    
    def __getitem__(self, idx):
        return self(idx)
